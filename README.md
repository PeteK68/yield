# Welcome to Yield #
The simplest ASP.NET static site generator (a.k.a "flattener"). A 10-minute investment is ALL that is required. If you are already coding ASP.NET websites, no additional investment in learning a new syntax. The beauty about using Yield is that it inherently scales up to support dynamic websites, simply by disabling Yield.

## Quickstart Guide ##
### To get started, simply follow these steps: ###

1. Code in ASP.NET as you normally would. 
1. Add the Yield package to your project from [NuGet](https://www.nuget.org/packages/Yield/).
1. Configure Yield by following the examples in the "YieldSample.Web.config" sample file.
    1. A full description of the settings can be found in the "[Sample Web.config file to enable Yield](https://bitbucket.org/PeteK68/yield/wiki/Web.config)" wiki page.
1. Make some minor tweaks to your code (as shown below).
1. Run your site.
1. Go to the crawler URL.
1. Test your static site.
1. Publish your static site.

## Web.config ##
Available web.config settings can be found in the "[Sample Web.config file to enable Yield](https://bitbucket.org/PeteK68/yield/wiki/Web.config)" wiki page.

## Code modifications ##
For a complete list of recommended code tweaks, see the "[Code Tweaks](https://bitbucket.org/PeteK68/yield/wiki/Code%20Tweaks)" page for more details.

## Disabling Yield ##
Simply remove or comment the Yield Module.

```
<configuration>
    ...
  <system.webServer>
    <modules>
      <!--<add name="Yield" type="Yield.YieldModule, Yield"/>-->
    </modules>
   ...
  </system.webServer>
</configuration>
```

## Viewing your static site in IIS Express ##
To properly test your flattened site, it's recommended that you view it from a web server.

Command Prompt:     
`"C:\Program Files (x86)\IIS Express\iisexpress.exe" /systray:true /path:C:\Users\Peter\Downloads\flattener-test\_output`

PowerShell:     
`& 'C:\Program Files (x86)\IIS Express\iisexpress.exe' /systray:true /path:'C:\Users\Peter\Downloads\flattener-test\_output'`