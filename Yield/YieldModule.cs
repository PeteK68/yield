﻿using System;
using System.Web;
using Yield.Flattener;
using Yield.Copier;
using System.Linq;
using System.Diagnostics;

namespace Yield
{
    public class YieldModule : IHttpModule
    {
        private HttpApplication httpApplication;
        private FileSystemWriter flattener;
        private IDirectoryCopier copier;

        public void Init(HttpApplication application)
        {
            httpApplication = application;
            httpApplication.BeginRequest += HttpApplicationBeginRequest;
            httpApplication.EndRequest += HttpApplicationEndRequest;
        }

        void HttpApplicationBeginRequest(object sender, EventArgs e)
        {
            // if the 'yield-flatten=0..n' cookie is present
            // or if settings are set to always-on
            if (YieldConfig.Flatten.On == "always" || httpApplication.Request.Cookies.AllKeys.Contains("yield-flatten"))
            {
                RemoveGzipEncoding();
                SetupFlattener();
                StartCopyTasks();
            }

            Debug.WriteLine($"Start: {httpApplication.Request.Url.AbsoluteUri}");
        }

        void HttpApplicationEndRequest(object sender, EventArgs e)
        {
            if (YieldConfig.Flatten.On == "always" || httpApplication.Request.Cookies.AllKeys.Contains("yield-flatten"))
            {
                ProcessEndRequest();
            }

            Debug.WriteLine($"End: {httpApplication.Request.Url.AbsoluteUri}");
            Debug.WriteLine("");
        }

        private void ProcessEndRequest()
        {
            flattener?.EndRequest();
            flattener = null;
        }

        private void StartCopyTasks()
        {
            if (YieldConfig.Tasks.ExecuteFirst)
            {
                if (copier == null) copier = new DirectoryCopier(httpApplication);
            }
            else
            {
                if (copier == null) copier = new DelayedDirectoryCopier(httpApplication);
            }

            copier.Execute();
        }

        private void SetupFlattener()
        {
            flattener = new FileSystemWriter(httpApplication);
        }

        private void RemoveGzipEncoding()
        {
            if (httpApplication.Request.Headers["Accept-Encoding"] != null)
            {
                httpApplication.Request.Headers.Remove("Accept-Encoding");
            }
        }

        public void Dispose()
        {
            ProcessEndRequest();
        }
    }
}
