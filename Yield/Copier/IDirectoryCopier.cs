﻿using System.Web;

namespace Yield.Copier
{
    public interface IDirectoryCopier
    {
        void Execute();
        HttpApplication HttpApplication { get; set; }
    }
}