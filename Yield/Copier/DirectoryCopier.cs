﻿using Yield.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Yield.Copier
{
    public class DirectoryCopier : IDirectoryCopier
    {
        private TasksCollection tasks = YieldConfig.Tasks;
        public HttpApplication HttpApplication { get; set; }

        public DirectoryCopier(HttpApplication httpApplication)
        {
            HttpApplication = httpApplication;
        }

        public virtual void Execute()
        {
            if (StateManager.StateManager.CopyExecuted == false) // only run the first time
            {
                StateManager.StateManager.CopyExecuted = true;

                foreach (TaskElement task in tasks)
                {
                    var source = YieldConfig.MakeAbsolute(AppDomain.CurrentDomain.BaseDirectory, task.Source);
                    var destination = YieldConfig.MakeAbsolute(task.Destination);

                    if (task.Action == "copy")
                    {
                        RecursiveCopyFolder(source, task.RegexFilePattern, task.SimpleFilePattern, destination);
                    }
                    else if (task.Action == "move")
                    {
                        RecursiveCopyFolder(source, task.RegexFilePattern, task.SimpleFilePattern, destination, move: true);
                    }
                }
            }
        }

        private int GetCookieValue(string cookieName)
        {
            int count;
            var value = int.TryParse(HttpApplication.Request.Cookies?[cookieName]?.Value, out count);

            return count;
        }

        public static void RecursiveCopyFolder(string sourceFolder, string regexPattern, string simplePattern, string destFolder, bool move = false)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            // Copy all files
            var files = GetFilesFromPattern(sourceFolder, regexPattern, simplePattern);
            foreach (var file in files)
            {
                var name = Path.GetFileName(file);
                var destination = Path.Combine(destFolder, name);

                if (move)
                    File.Move(file, destination);
                else
                    File.Copy(file, destination, overwrite: true);
            }

            // Copy all folders, recursively
            var folders = Directory.GetDirectories(sourceFolder);
            foreach (var folder in folders)
            {
                var name = Path.GetFileName(folder);
                var dest = Path.Combine(destFolder, name);

                RecursiveCopyFolder(folder, regexPattern, simplePattern, dest, move);
            }
        }

        private static string[] GetFilesFromPattern(string sourceFolder, string regexPattern, string simplePattern)
        {
            string[] files;

            if (string.IsNullOrEmpty(simplePattern))
            {
                var filePattern = new Regex(regexPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                files = Directory.GetFiles(sourceFolder).Where(fn => filePattern.IsMatch(fn)).ToArray();
            }
            else
            {
                files = Directory.GetFiles(sourceFolder, simplePattern);
            }

            return files;
        }

        private static string GetSourcePath(string sourceFolder)
        {
            var path = string.Empty;

            if (Directory.Exists(sourceFolder))
            {
                path = sourceFolder;
            }
            else
            {
                path = Path.GetDirectoryName(sourceFolder);
            }

            return path;
        }

        private static string GetSourcePattern(string sourceFolder)
        {
            var pattern = string.Empty;

            if (sourceFolder.Contains(""))
            {
                pattern = "*.*";
            }
            else
            {
                pattern = sourceFolder.Split('\\').Last();
            }

            return pattern;
        }
    }
}
