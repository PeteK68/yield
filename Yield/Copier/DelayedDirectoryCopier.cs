﻿using Yield.Configuration;
using System.Timers;
using System.Web;

namespace Yield.Copier
{
    public class DelayedDirectoryCopier : DirectoryCopier
    {
        private TasksCollection tasks = YieldConfig.Tasks;
        private static Timer timer = new Timer();
        private const int delay = 2000;

        public DelayedDirectoryCopier(HttpApplication httpApplication) : base (httpApplication) { }

        public override void Execute()
        {
            ResetTimer();
        }

        private void ResetTimer()
        {
            timer.Stop();
            timer.AutoReset = false;
            timer.Interval = delay;
            timer.Elapsed -= TimerElapsed;
            timer.Elapsed += TimerElapsed;
            timer.Start();
        }

        void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            timer.Close();

            base.Execute();
        }
    }
}
