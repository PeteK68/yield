﻿using System.Timers;

namespace Yield.StateManager
{
    public static class StateManager
    {
        public static bool CopyExecuted { get { return isRunning; } set { ResetTimer(value); } }

        private static Timer timer = new Timer();
        private const int delay = 5000;
        private static bool isRunning = false;

        private static void ResetTimer(bool value)
        {
            if (value == true)
            {
                isRunning = value;

                timer.Stop();
                timer.AutoReset = false;
                timer.Interval = delay;
                timer.Elapsed -= TimerElapsed;
                timer.Elapsed += TimerElapsed;

                timer.Start();
            }
            else
            {
                TimerElapsed(null, null);
            }
        }

        private static void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            timer.Close();

            isRunning = false;
        }
    }
}
