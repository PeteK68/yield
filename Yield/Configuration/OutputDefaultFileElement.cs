﻿using System.Configuration;

namespace Yield.Configuration
{
    public class OutputDefaultFileElement : ConfigurationElement
    {
        private static readonly ConfigurationProperty filename = new ConfigurationProperty("filename", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);

        public OutputDefaultFileElement()
        {
            base.Properties.Add(filename);
        }

        [ConfigurationProperty("filename", IsRequired = true)]
        public string Filename { get { return (string)this[filename]; } }
    }
}