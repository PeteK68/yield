﻿using System.Configuration;

namespace Yield.Configuration
{
    public class LinkElement : ConfigurationElement
    {
        private static readonly ConfigurationProperty value = new ConfigurationProperty("value", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);

        public LinkElement()
        {
            base.Properties.Add(value);
        }

        [ConfigurationProperty("value", DefaultValue = "copy", IsRequired = true)]
        public string Value { get { return (string)this[value]; } }
    }
}