﻿using System.Configuration;

namespace Yield.Configuration
{

    [ConfigurationCollection(typeof(TaskElement), AddItemName = "task", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class TasksCollection : ConfigurationElementCollection
    {
        private static readonly ConfigurationProperty executeFirst = new ConfigurationProperty("executeFirst", typeof(string), string.Empty, ConfigurationPropertyOptions.None);

        protected override ConfigurationElement CreateNewElement()
        {
            return new TaskElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var e = (TaskElement)element;
            return e.Action + e.Source + e.Destination;
        }

        new public TaskElement this[string name]
        {
            get { return (TaskElement)BaseGet(name); }
        }

        [ConfigurationProperty("executeFirst", DefaultValue = "true", IsRequired = false)]
        public bool ExecuteFirst { get { return (bool)this[executeFirst]; } }
    }
}