﻿using System.Configuration;

namespace Yield.Configuration
{
    public class OutputFolderElement : ConfigurationElement
    {
        private static readonly ConfigurationProperty path = new ConfigurationProperty("path", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);

        public OutputFolderElement()
        {
            base.Properties.Add(path);
        }

        [ConfigurationProperty("path", IsRequired = false)]
        public string Path { get { return (string)this[path]; } }
    }
}