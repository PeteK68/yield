﻿using System.Configuration;

namespace Yield.Configuration
{
    public class FlattenElement : ConfigurationElement
    {
        private static readonly ConfigurationProperty on = new ConfigurationProperty("on", typeof(string), "crawl-only", ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty filename = new ConfigurationProperty("filename", typeof(string), "index.html", ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty destination = new ConfigurationProperty("destination", typeof(string), "", ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty convertLinksToRelative = new ConfigurationProperty("convertLinksToRelative", typeof(string), "false", ConfigurationPropertyOptions.None);

        public FlattenElement()
        {
            base.Properties.Add(on);
            base.Properties.Add(filename);
            base.Properties.Add(destination);
            base.Properties.Add(convertLinksToRelative);
        }

        [ConfigurationProperty("on", IsRequired = true)]
        public string On { get { return (string)this[on]; } }

        [ConfigurationProperty("filename", IsRequired = true)]
        public string Filename { get { return (string)this[filename]; } }

        [ConfigurationProperty("destination", IsRequired = true)]
        public string Destination { get { return (string)this[destination]; } }

        [ConfigurationProperty("convertLinksToRelative", IsRequired = false)]
        public bool ConvertToRelativeLinks { get { return (bool)this[convertLinksToRelative]; } }
    }
}