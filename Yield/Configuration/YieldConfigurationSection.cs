﻿using System.Configuration;

namespace Yield.Configuration
{
    public class YieldConfigurationSection : ConfigurationSection
    {
        private static readonly ConfigurationProperty tasksList = new ConfigurationProperty("tasks", typeof(TasksCollection), null);
        private static readonly ConfigurationProperty linksList = new ConfigurationProperty("links", typeof(LinksCollection), null);
        private static readonly ConfigurationProperty outputFolderElement = new ConfigurationProperty("outputFolder", typeof(OutputFolderElement), null, ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty flattenElement = new ConfigurationProperty("flatten", typeof(FlattenElement), null);

        private static YieldConfigurationSection configSettings = ConfigurationManager.GetSection("yield") as YieldConfigurationSection;
        public static YieldConfigurationSection Configuration { get { return configSettings; } }

        public YieldConfigurationSection()
        {
            base.Properties.Add(tasksList);
            base.Properties.Add(linksList);
            base.Properties.Add(outputFolderElement);
            base.Properties.Add(flattenElement);
        }

        [ConfigurationProperty("flatten", IsRequired = false)]
        public FlattenElement Flatten
        {
            get { return (FlattenElement)this[flattenElement]; }
        }

        [ConfigurationProperty("tasks", IsRequired = false)]
        public TasksCollection Tasks
        {
            get { return (TasksCollection)this[tasksList]; }
        }

        [ConfigurationProperty("links", IsRequired = false)]
        public LinksCollection Links
        {
            get { return (LinksCollection)this[linksList]; }
        }

        [ConfigurationProperty("outputFolder", IsRequired = true)]
        public OutputFolderElement OutputFolder
        {
            get { return (OutputFolderElement)this[outputFolderElement]; }
        }
    }
}