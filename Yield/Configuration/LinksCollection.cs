﻿using System.Configuration;

namespace Yield.Configuration
{
    [ConfigurationCollection(typeof(LinkElement), AddItemName = "link", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class LinksCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LinkElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var e = (LinkElement)element;
            return e.Value;
        }

        new public LinkElement this[string name]
        {
            get { return (LinkElement)BaseGet(name); }
        }
    }
}