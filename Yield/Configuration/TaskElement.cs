﻿using System.Configuration;

namespace Yield.Configuration
{
    public class TaskElement : ConfigurationElement
    {
        private static readonly ConfigurationProperty action = new ConfigurationProperty("action", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty source = new ConfigurationProperty("source", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty destination = new ConfigurationProperty("destination", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired);
        private static readonly ConfigurationProperty regexPattern = new ConfigurationProperty("regexPattern", typeof(string), ".*");
        private static readonly ConfigurationProperty simplePattern = new ConfigurationProperty("simplePattern", typeof(string), string.Empty);

        public TaskElement()
        {
            base.Properties.Add(action);
            base.Properties.Add(source);
            base.Properties.Add(destination);
            base.Properties.Add(regexPattern);
            base.Properties.Add(simplePattern);
        }

        [ConfigurationProperty("action", DefaultValue = "copy", IsRequired = true)]
        public string Action { get { return (string)this[action]; } }

        [ConfigurationProperty("source", IsRequired = true)]
        public string Source { get { return (string)this[source]; } }

        [ConfigurationProperty("destination", IsRequired = true)]
        public string Destination { get { return (string)this[destination]; } }

        [ConfigurationProperty("regexPattern", IsRequired = false, DefaultValue = ".*")]
        public string RegexFilePattern { get { return (string)this[regexPattern]; } }

        [ConfigurationProperty("simplePattern", IsRequired = false)]
        public string SimpleFilePattern { get { return (string)this[simplePattern]; } }
    }
}