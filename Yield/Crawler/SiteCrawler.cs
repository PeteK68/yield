﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Yield.Configuration;

namespace Yield.Crawler
{
    public class SiteCrawler
    {
        private HttpContext context;
        private Dictionary<string, bool> crawlList = new Dictionary<string, bool>(); // url, processed flag
        private Uri baseUrl;

        public SiteCrawler(HttpContext context)
        {
            this.context = context;
            baseUrl = UrlResolver.MakeAbsolute(Uri.UriSchemeHttp + "://" + context.Request.Url.Authority);

            AddConfigLinksToCrawlList();
        }

        private void AddConfigLinksToCrawlList()
        {
            foreach (LinkElement link in YieldConfig.Links)
            {
                var url = UrlResolver.ConvertToFullyQualifiedUrl(baseUrl.AbsoluteUri, link.Value).AbsoluteUri;
                AddLinkToCrawl(url);
            }
        }

        private void AddLinksToCrawl(IEnumerable<string> links)
        {
            var newCrawlUrls = links.ToDictionary(s => s, s => false);

            // make sure no duplicates get added
            crawlList = crawlList.Concat(newCrawlUrls.Where(kv => !crawlList.ContainsKey(kv.Key)))
                                 .ToDictionary(c => c.Key, c => c.Value);
        }

        private void AddLinkToCrawl(string link)
        {
            if (!crawlList.Any(s => s.Key == link))
            {
                crawlList.Add(link, false);
            }
        }

        public void Start()
        {
            var response = MakeRequest(baseUrl);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var htmlContent = ReadResponseData(response);
                var newBase = UrlFinder.GetBase(htmlContent);
                baseUrl = UrlResolver.ConvertToFullyQualifiedUrl(baseUrl, newBase);

                var urls = GetUrlsFromHtml(baseUrl.AbsoluteUri, htmlContent);
                AddLinksToCrawl(urls);

                var start = DateTime.Now;

                do
                {
                    ProcessQueue(baseUrl);
                } while (crawlList.Where(kv => kv.Value == false).Count() > 0);

                var end = DateTime.Now;
                var span = (end - start).TotalMilliseconds;
                context.Response.Write("<h1>Total time: " + span + "</h1><br /><hr />");
            }
        }

        private List<string> GetUrlsFromHtml(string baseUrl, string htmlContent)
        {
            var urls = UrlFinder.FindHtmlLinks(htmlContent);
            urls = UrlResolver.ConvertToFullyQualifiedUrls(urls, baseUrl);

            return urls;
        }

        private void ProcessQueue(Uri baseUrl)
        {
            // base url of the current request
            var list = crawlList.Where(kv => kv.Value == false).ToList();
            foreach (var queueItem in list)
            {
                var url = new Uri(queueItem.Key);
                var processed = queueItem.Value;

                var start = DateTime.Now;

                var response = MakeRequest(url);
                context.Response.Write("Processed: " + url + "<br />");
                context.Response.Write("Returned Status: " + (int)response.StatusCode + " " + response.StatusCode + "<br />");

                if (response.StatusCode == HttpStatusCode.OK && response.ContentType == "text/html")
                {
                    var htmlContent = ReadResponseData(response);
                    var moreUrls = GetUrlsFromHtml(baseUrl.AbsoluteUri, htmlContent);

                    context.Response.Write("Discovered and added " + moreUrls.Count + " more URLs<br />");

                    moreUrls = UrlResolver.ConvertToFullyQualifiedUrls(moreUrls, url.AbsoluteUri);
                    AddLinksToCrawl(moreUrls);
                }

                crawlList[queueItem.Key] = true;

                var end = DateTime.Now;
                var span = (end - start).TotalMilliseconds;
                context.Response.Write("Time to process: " + span + "<br /><hr />");
            }
        }

        private HttpWebResponse MakeRequest(Uri url)
        {
            HttpWebResponse response;

            try
            {
                var hwreq = (HttpWebRequest)WebRequest.Create(url);
                hwreq.Headers.Add(HttpRequestHeader.Cookie, "yield-flatten=true");
                response = (HttpWebResponse)hwreq.GetResponse();
            }
            catch (WebException e) 
            {
                response = (HttpWebResponse)e.Response;
            }

            return response;
        }

        private string ReadResponseData(HttpWebResponse response)
        {
            // Handles multi-chunk responses. ReadToEnd() does not.
            var sb = new StringBuilder();
            var buf = new byte[8192];
            var count = 0;
            var resStream = response.GetResponseStream();

            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    sb.Append(Encoding.UTF8.GetString(buf, 0, count));
                }

            } while (count > 0);

            return sb.ToString();
        }
    }
}
