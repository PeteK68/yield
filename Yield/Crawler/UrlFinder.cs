﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Yield.Crawler
{
    public class UrlFinder
    {
        private static List<Tag> htmlTags;

        static UrlFinder()
        {
            LoadTags();
        }

        public static string GetBase(string source)
        {
            var tags = new List<Tag>()
            {
                new Tag { Name = "base", Attribute = "href" }
            };

            return FindWithinHtml(source, tags).FirstOrDefault();
        }

        public static List<string> FindHtmlLinks(string source)
        {
            return FindWithinHtml(source, htmlTags).Distinct().ToList();
        }

        public static List<string> FindCssLinks(string source)
        {
            return FindWithinCss(source).Distinct().ToList();
        }

        public static List<string> FindWithinCss(string source)
        {
            var list = new List<string>();
            
            // match ": url(...)"
            var matches = Regex.Matches(source, @":[ ]*url[ ]*\((['""]?)([^\)]*)\1\)");
            foreach (Match match in matches)
            {
                if (match.Groups.Count > 2)
                {
                    list.Add(match.Groups[2].Value);
                }
            }

            return list;
        }

        public static List<string> FindWithinHtml(string source, List<Tag> tags)
        {
            var list = new List<string>();

            foreach (var tag in tags)
            {
                // opening tag <
                // tag name {0}
                // at least one space
                // anything that's not a closing of a tag >, zero or more
                // attribute
                // zero or more spaces
                // = sign
                // zero or more spaces
                // zero or one ' or a " (1)
                // the value (anything except >) (2)
                // backreference 1
                // at least one space
                // anything that's not a closing of a tag >, zero or more
                // closing tag >
                var matches = Regex.Matches(source,
                                            string.Format(@"<{0}[ ]+[^>]*{1}[ ]*=[ ]*(['""])((?:(?!\1).)*)\1[ ]*[^>]*>",
                                                          tag.Name,
                                                          tag.Attribute));
                foreach (Match match in matches)
                {
                    if (match.Groups.Count > 2)
                    {
                        list.Add(match.Groups[2].Value);
                    }
                }
            }

            return list;
        }

        private static void LoadTags()
        {
            htmlTags = new List<Tag>()
            {
                new Tag { Name = "a", Attribute = "href" },
                new Tag { Name = "applet", Attribute = "codebase" },
                new Tag { Name = "area", Attribute = "href" },
                //new Tag { Name = "base", Attribute = "href" }, // Not needed here, as it is provided as the root of all other links
                new Tag { Name = "blockquote", Attribute = "cite" },
                new Tag { Name = "body", Attribute = "background" },
                new Tag { Name = "del", Attribute = "cite" },
                new Tag { Name = "form", Attribute = "action" },
                new Tag { Name = "frame", Attribute = "longdesc" },
                new Tag { Name = "frame", Attribute = "src" },
                new Tag { Name = "head", Attribute = "profile" },
                new Tag { Name = "iframe", Attribute = "longdesc" },
                new Tag { Name = "iframe", Attribute = "src" },
                new Tag { Name = "img", Attribute = "longdesc" },
                new Tag { Name = "img", Attribute = "src" },
                new Tag { Name = "img", Attribute = "usemap" },
                new Tag { Name = "input", Attribute = "src" },
                new Tag { Name = "input", Attribute = "usemap" },
                new Tag { Name = "ins", Attribute = "cite" },
                new Tag { Name = "link", Attribute = "href" },
                new Tag { Name = "object", Attribute = "classid" },
                new Tag { Name = "object", Attribute = "codebase" },
                new Tag { Name = "object", Attribute = "data" },
                new Tag { Name = "object", Attribute = "usemap" },
                new Tag { Name = "q", Attribute = "cite" },
                new Tag { Name = "script", Attribute = "src" },
                new Tag { Name = "audio", Attribute = "src" },
                new Tag { Name = "button", Attribute = "formaction" },
                new Tag { Name = "command", Attribute = "icon" },
                new Tag { Name = "embed", Attribute = "src" },
                new Tag { Name = "html", Attribute = "manifest" },
                new Tag { Name = "input", Attribute = "formaction" },
                new Tag { Name = "source", Attribute = "src" },
                new Tag { Name = "video", Attribute = "poster" },
                new Tag { Name = "video", Attribute = "src" }
            };
        }
    }
}
