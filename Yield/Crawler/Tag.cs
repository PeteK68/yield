﻿namespace Yield.Crawler
{
    public struct Tag
    {
        public string Name { get; set; }
        public string Attribute { get; set; }
    }
}
