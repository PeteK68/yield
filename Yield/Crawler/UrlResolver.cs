﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Yield.Crawler
{
    static class UrlResolver
    {
        internal static List<string> ConvertToFullyQualifiedUrls(IEnumerable<string> urls, string requestUrl)
        {
            var baseUrl = new Uri(requestUrl);
            var newList = new List<string>();

            var acceptedList = new List<string> { "http:", "https:" };

            foreach (var url in urls.HasValidScheme(true, acceptedList))
            {
                var newUrl = ConvertToFullyQualifiedUrl(requestUrl, url).AbsoluteUri;
                var newUrlhost = new Uri(newUrl).Host;

                if (newUrlhost == baseUrl.Host)
                    newList.Add(newUrl);
            }

            return newList.Distinct().ToList();
        }

        internal static Uri ConvertToFullyQualifiedUrl(Uri baseUrl, string url)
        {
            var uri = new Uri(baseUrl, url);
            return uri;
        }

        internal static Uri ConvertToFullyQualifiedUrl(string baseUrl, string url)
        {
            var baseUri = new Uri(baseUrl);
            return ConvertToFullyQualifiedUrl(baseUri, url);
        }

        internal static bool IsAbsolute(string url)
        {
            Uri result;
            Uri.TryCreate(url, UriKind.Absolute, out result);

            return result?.IsAbsoluteUri ?? false;
        }

        internal static IEnumerable<string> HasValidScheme(this IEnumerable<string> values, bool includeNoScheme, IEnumerable<string> listOfOptions)
        {
            foreach (string value in values)
            {
                foreach (string option in listOfOptions)
                {
                    if (value.StartsWith(option) || (includeNoScheme && !value.Contains(":")))
                        yield return value;
                }
            }
        }

        internal static Uri MakeAbsolute(string url)
        {
            return new Uri(url);
        }

        internal static string MakeRelative(Uri baseUrl, string url)
        {
            var absolute = ConvertToFullyQualifiedUrl(baseUrl, url);
            return MakeRelative(baseUrl, absolute);
        }

        internal static string MakeRelative(Uri baseUrl, Uri url)
        {
            var relative = baseUrl.MakeRelativeUri(url);
            return relative.OriginalString;
        }

        internal static Uri NormalizeUrl(Uri url)
        {
            if (!url.Segments.Last().Contains(".") && !url.AbsolutePath.EndsWith("/"))
            {
                url = new Uri(url.OriginalString + "/");
            }

            return url;
        }
    }
}
