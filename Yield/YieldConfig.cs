﻿using Yield.Configuration;
using System;
using System.IO;
using System.Linq;

namespace Yield
{
    public static class YieldConfig
    {
        private static YieldConfigurationSection config;

        public static string OutputFolder { get; private set; }
        public static FlattenElement Flatten { get; private set; }
        public static TasksCollection Tasks { get; private set; }
        public static LinksCollection Links { get; private set; }

        static YieldConfig()
        {
            config = YieldConfigurationSection.Configuration;
            Tasks = config.Tasks;
            Flatten = config.Flatten;
            Links = config.Links;

            SetOutputFolder();
        }

        private static void SetOutputFolder()
        {
            var folder = config.OutputFolder.Path;

            OutputFolder = MakeAbsolute(folder);
        }

        public static string MakeAbsolute(params string[] pathParts)
        {
            pathParts = pathParts.Select(part => part.Replace("/", @"\").TrimStart('\\').TrimEnd('\\')).ToArray();

            if (!pathParts[0].Contains(":"))
            {
                var parts = pathParts.ToList();

                if (string.IsNullOrEmpty(OutputFolder))
                {
                    parts.Insert(0, AppDomain.CurrentDomain.BaseDirectory);                   
                }
                else
                {
                    parts.Insert(0, OutputFolder);
                }

                pathParts = parts.ToArray();
            }

            return Path.GetFullPath(Path.Combine(pathParts));
        }
    }
}
