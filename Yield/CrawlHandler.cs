﻿using System.Web;
using Yield.Crawler;

namespace Yield
{
    public class CrawlHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var crawler = new SiteCrawler(context);
            crawler.Start();
        }
    }
}
