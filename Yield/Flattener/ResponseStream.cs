﻿using System.IO;

namespace Yield.Flattener
{
    public delegate void StreamWrittenEventHandler(object sender, StreamWrittenEventArgs e);

    public class ResponseStream : Stream
    {
        private Stream stream;
        private long length;
        private long position;

        public event StreamWrittenEventHandler BeforeStreamWritten;
        public event StreamWrittenEventHandler AfterStreamWritten;

        #region Properties
        public override bool CanRead { get { return false; } }
        public override bool CanSeek { get { return true; } }
        public override bool CanWrite { get { return true; } }
        public override long Length { get { return length; } }
        public override long Position { get { return position; } set { position = value; } }
        #endregion

        public ResponseStream(Stream stream)
        {
            this.stream = stream;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return stream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin direction)
        {
            return stream.Seek(offset, direction);
        }

        public override void SetLength(long length)
        {
            this.length = length;
        }

        public override void Flush()
        {
            stream.Flush();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            BeforeStreamWritten?.Invoke(this, new StreamWrittenEventArgs() { Buffer = buffer, Offset = offset, Count = count });

            stream.Write(buffer, offset, count);

            AfterStreamWritten?.Invoke(this, new StreamWrittenEventArgs() { Buffer = buffer, Offset = offset, Count = count });
        }
    }
}
