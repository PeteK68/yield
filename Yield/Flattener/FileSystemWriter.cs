﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using Yield.Crawler;

namespace Yield.Flattener
{
    public class FileSystemWriter
    {
        private FileStream fileWriter;
        private FileNamer namer;
        private HttpApplication httpApplication;
        private const string cssPattern = @"url[ ]*\((|['""]*){0}\1*\)";
        private const string cssReplace = @"url($1{0}$1)";
        private const string htmlPattern = @"(['""]){0}\1"; 
        private const string htmlReplace = @"$1{0}$1";

        public FileSystemWriter(HttpApplication app)
        {
            httpApplication = app;

            var stream = new ResponseStream(httpApplication.Response.Filter);
            stream.BeforeStreamWritten += new StreamWrittenEventHandler(BeforeStreamWritten);
            httpApplication.Response.Filter = stream;
        }

        internal void EndRequest()
        {
            CloseFile();

            if (YieldConfig.Flatten.ConvertToRelativeLinks && namer != null)
            {
                if (httpApplication.Response.ContentType.Contains("text"))
                {
                    var content = File.ReadAllText(namer.DestinationFilePath);

                    if (httpApplication.Response.ContentType == "text/html")
                    {
                        // html path replacements
                        var baseUrl = UrlResolver.NormalizeUrl(httpApplication.Request.Url);
                        var urls = UrlFinder.FindHtmlLinks(content);

                        content = processLinks(content, baseUrl, urls, htmlPattern, htmlReplace);

                        // in-line css replacements
                        Uri requestUri;
                        var filename = Path.GetFileName(httpApplication.Request.Url.AbsoluteUri);
                        if (string.IsNullOrEmpty(filename))
                        {
                            Uri.TryCreate(httpApplication.Request.Url.AbsoluteUri, UriKind.RelativeOrAbsolute, out requestUri);
                        }
                        else
                        {
                            Uri.TryCreate(httpApplication.Request.Url.AbsoluteUri.Replace(filename, ""), UriKind.RelativeOrAbsolute, out requestUri);
                        }

                        var cssBaseUrl = UrlResolver.NormalizeUrl(requestUri);
                        var cssUrls = UrlFinder.FindCssLinks(content);

                        content = processLinks(content, cssBaseUrl, cssUrls, cssPattern, cssReplace);
                    }
                    else if (httpApplication.Response.ContentType == "text/css")
                    {
                        Uri requestUri;
                        Uri.TryCreate(httpApplication.Request.Url.AbsoluteUri.Replace(Path.GetFileName(httpApplication.Request.Url.AbsoluteUri), ""), UriKind.RelativeOrAbsolute, out requestUri);

                        var baseUrl = UrlResolver.NormalizeUrl(requestUri);
                        var urls = UrlFinder.FindCssLinks(content);

                        content = processLinks(content, baseUrl, urls, cssPattern, cssReplace);
                    }

                    File.WriteAllText(namer.DestinationFilePath, content);
                }
            }
        }

        private static string processLinks(string content, Uri baseUrl, List<string> urls, string regexPattern, string regexReplacement)
        {
            foreach (var url in urls)
            {
                if (!UrlResolver.IsAbsolute(url))
                {
                    var tagUrl = UrlResolver.NormalizeUrl(UrlResolver.ConvertToFullyQualifiedUrl(baseUrl, url));
                    var newUrl = UrlResolver.MakeRelative(baseUrl, tagUrl);

                    var pattern = string.Format(regexPattern, url);
                    var replace = string.Format(regexReplacement, newUrl);
                    content = Regex.Replace(content, pattern, replace, RegexOptions.IgnoreCase);
                }
            }

            return content;
        }

        private void BeforeStreamWritten(object sender, StreamWrittenEventArgs e)
        {
            // must open the file for writing after the response stream has begun
            // otherwise content type is unknown
            OpenFile();
            fileWriter.Write(e.Buffer, e.Offset, e.Count);
            //httpApplication.Response.Filter.CopyTo(fileWriter);
        }

        private void OpenFile()
        {
            if (fileWriter == null)
            {
                namer = new FileNamer(httpApplication.Request.Path, httpApplication.Request.PhysicalPath, httpApplication.Response.ContentType);
                namer.CreateFolderStructure();

                if (File.Exists(namer.DestinationFilePath))
                {
                    File.Delete(namer.DestinationFilePath);
                }

                fileWriter = new FileStream(namer.DestinationFilePath, FileMode.Create);

                httpApplication.Response.CacheControl = "no-cache";
            }
        }

        private void CloseFile()
        {
            if (fileWriter != null)
            {
                fileWriter.Close();
                fileWriter = null;
            }
        }
    }
}
