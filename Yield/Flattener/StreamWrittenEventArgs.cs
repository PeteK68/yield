﻿using System;

namespace Yield.Flattener
{
    public class StreamWrittenEventArgs : EventArgs
    {
        public byte[] Buffer { get; set; }
        public int Offset { get; set; }
        public int Count { get; set; }
    }
}
