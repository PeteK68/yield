﻿using System.IO;

namespace Yield.Flattener
{
    public class FileNamer
    {
        public string Filename { get; set; }
        public string BasePath { get; set; }
        public string DestinationDirectoryPath { get; set; }
        public string DestinationFilePath { get; set; }

        public FileNamer(string requestUri, string physicalPath, string contentType)
        {
            BasePath = YieldConfig.OutputFolder;

            if (File.Exists(physicalPath))
            {
                UseFileNaming(requestUri);
            }
            else if (contentType == "text/html")
            {
                UseDirectoryNaming(requestUri);
            }
            else
            {
                UseFileNaming(requestUri);
            }

            DestinationFilePath = Path.Combine(DestinationDirectoryPath, Filename);
        }

        private void UseFileNaming(string requestUri)
        {
            Filename = Path.GetFileName(requestUri);
            var requestPath = requestUri.TrimStart('\\');
            var subFolder = YieldConfig.Flatten.Destination.TrimStart('\\');

            DestinationDirectoryPath = YieldConfig.MakeAbsolute(subFolder, Path.GetDirectoryName(requestPath));
        }

        private void UseDirectoryNaming(string requestUri)
        {
            Filename = YieldConfig.Flatten.Filename;
            var requestPath = requestUri.TrimStart('\\');
            var subFolder = YieldConfig.Flatten.Destination.TrimStart('\\');

            DestinationDirectoryPath = YieldConfig.MakeAbsolute(subFolder, requestPath);
        }

        public void CreateFolderStructure()
        {
            Directory.CreateDirectory(DestinationDirectoryPath);
        }
    }
}
